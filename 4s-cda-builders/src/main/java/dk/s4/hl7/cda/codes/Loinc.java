package dk.s4.hl7.cda.codes;

//CHECKSTYLE:OFF
public class Loinc {
  public static final String OID = "2.16.840.1.113883.6.1";
  public static final String DISPLAYNAME = "LOINC";

  public static final String PHMR_CODE = "53576-5";
  public static final String PMHR_DISPLAYNAME = "Personal Health Monitoring Report";

  public static final String SECTION_VITAL_SIGNS_CODE = "8716-3";
  public static final String SECTION_RESULTS_CODE = "30954-2";
  public static final String SECTION_MEDICAL_CODE = "46264-8";
  public static final String SECTION_QRD_CODE = "74467-2";

  public static final String COMMENT = "48767-8";
  public static final String COMMENT_DISPLAYNAME = "Kommentar til måling";

  public static final String QFD_CODE = "74468-0";
  public static final String QFD_DISPLAYNAME = "Form Definition Document";

  public static final String QRD_CODE = "74465-6";
  public static final String QRD_DISPLAYNAME = "Questionnaire Response Document";
  
  public static final String FEEDBACK_CODE = "4466-4";
  public static final String FEEDBACK_DISPLAYNAME = "Feedback to user post question response Question";
  
  public static final String HELPTEXT_CODE = "48767-8";
  public static final String HELPTEXT_DISPLAYNAME = "Annotation Comment";

}
