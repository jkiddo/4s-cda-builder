package dk.s4.hl7.cda.codes;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;

//CHECKSTYLE:OFF
public class NSI {
  public static final String SOR_AUTHORITYNAME = "SOR";
  public static final String SOR_OID = "1.2.208.176.1.1";

  public static final String CPR_AUTHORITYNAME = "CPR";
  public static final String CPR_OID = "1.2.208.176.1.2";

  public static final String YDER_AUTHORITYNAME = "YDER";
  public static final String YDER_OID = "1.2.208.176.1.4";

  public static ID createSOR(String sor) {
    return new IDBuilder().setAuthorityName(SOR_AUTHORITYNAME).setRoot(SOR_OID).setExtension(sor).build();
  }

  public static ID createYDER(String yder) {
    return new IDBuilder().setAuthorityName(YDER_AUTHORITYNAME).setRoot(YDER_OID).setExtension(yder).build();
  }

  public static ID createCPR(String cpr) {
    return new IDBuilder().setAuthorityName(CPR_AUTHORITYNAME).setRoot(CPR_OID).setExtension(cpr).build();
  }
}
