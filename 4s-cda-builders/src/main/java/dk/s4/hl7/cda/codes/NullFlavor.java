package dk.s4.hl7.cda.codes;

/**
 * Null flavors in HL7
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public enum NullFlavor {
  NO_INFORMATION
}
