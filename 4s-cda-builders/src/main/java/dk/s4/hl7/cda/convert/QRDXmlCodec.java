package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec, to encode and decode QRD from objects to XML and
 * XML to objects. When after construction the codec is considered thread-safe.
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 * @author Frank Jacobsen Systematic
 */

public class QRDXmlCodec implements Codec<QRDDocument, String>, AppendableSerializer<QRDDocument>,
    ReaderSerializer<QRDDocument> {
  private QRDXmlConverter qrdXmlConverter;
  private XmlQRDConverter xmlQrdConverter;

  public QRDXmlCodec() {
    this(new XmlPrettyPrinter());
  }

  public QRDXmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.qrdXmlConverter = new QRDXmlConverter(xmlPrettyPrinter);
    this.xmlQrdConverter = new XmlQRDConverter();
  }

  public String encode(QRDDocument source) {
    return qrdXmlConverter.convert(source);
  }

  @Override
  public void serialize(QRDDocument source, Appendable target) {
    qrdXmlConverter.serialize(source, target);
  }

  public QRDDocument decode(String source) {
    return xmlQrdConverter.convert(source);

  }

  @Override
  public QRDDocument deserialize(Reader source) {
    return xmlQrdConverter.deserialize(source);
  }
}
