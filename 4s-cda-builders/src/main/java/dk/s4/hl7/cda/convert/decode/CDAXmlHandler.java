package dk.s4.hl7.cda.convert.decode;

import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XmlHandler;

public interface CDAXmlHandler<E extends ClinicalDocument> extends XmlHandler {
  void addDataToDocument(E clinicalDocument);
}
