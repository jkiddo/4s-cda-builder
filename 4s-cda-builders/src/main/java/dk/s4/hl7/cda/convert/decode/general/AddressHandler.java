package dk.s4.hl7.cda.convert.decode.general;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.AddressBuilder;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class AddressHandler extends BaseXmlHandler {
  private String addressElementName;
  private AddressBuilder addressBuilder;
  private Use defaultUse;

  public AddressHandler(String addressParentXPath, String addressElementName, Use defaultUse) {
    this.addressElementName = addressElementName;
    this.defaultUse = defaultUse;
    String baseElementPath = addressParentXPath + "/" + addressElementName;
    addPath(baseElementPath);
    addPath(baseElementPath + "/streetAddressLine");
    addPath(baseElementPath + "/postalCode");
    addPath(baseElementPath + "/city");
    addPath(baseElementPath + "/country");
  }
  
  public AddressHandler(String xpathToParentElement, Use defaultUse) {
    this(xpathToParentElement, "addr", defaultUse);
  }

  public AddressData getAddress() {
    if (addressBuilder != null) {
      return addressBuilder.build();
    }
    return null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), addressElementName)) {
      addressBuilder = new AddressBuilder();
      addressBuilder.setUse(ConvertXmlUtil.getUse(xmlElement, defaultUse));
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "streetAddressLine")) {
      addressBuilder.addAddressLine(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "postalCode")) {
      addressBuilder.setPostalCode(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "city")) {
      addressBuilder.setCity(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "country")) {
      addressBuilder.setCountry(xmlElement.getElementValue());
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
  }
  
  public void clear() {
    addressBuilder = null;
  }
}
