package dk.s4.hl7.cda.convert.decode.header;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.AddressHandler;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.general.OrganizationHandler;
import dk.s4.hl7.cda.convert.decode.general.PersonHandler;
import dk.s4.hl7.cda.convert.decode.general.TelecomHandler;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Participant.ROLE_TYPE;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ParticipantHandler extends BaseXmlHandler implements CDAXmlHandler<ClinicalDocument> {
  private static final Logger logger = LoggerFactory.getLogger(ParticipantHandler.class);

  private String baseElementName;
  private ROLE_TYPE roleType;
  private IdHandler idHandler;
  private AddressHandler addressHandler;
  private TelecomHandler telecomHandler;
  private Date time;
  private PersonHandler personHandler;
  private OrganizationHandler organizationHandler;
  private ParticipantType participantType;
  private List<Participant> participants;

  public enum ParticipantType {
    AUTHOR, LEGAL_AUTHENTICATOR, DATAENTERER, INFORMATION_RECIPIENT, PARTICIPANT
  }

  public ParticipantHandler(ParticipantType participantType, String completeBaseXPath, String completeTimeXPath,
      String personElementName, String organizationXPath) {
    this.participantType = participantType;
    addPath(completeTimeXPath);
    addPath(completeBaseXPath);
    this.baseElementName = findBaseElementName(completeBaseXPath);
    idHandler = new IdHandler(completeBaseXPath);
    addressHandler = new AddressHandler(completeBaseXPath, Use.WorkPlace);
    telecomHandler = new TelecomHandler(completeBaseXPath);
    personHandler = new PersonHandler(completeBaseXPath, personElementName);
    organizationHandler = new OrganizationHandler(completeBaseXPath + organizationXPath);
    participants = new ArrayList<Participant>();
  }

  private String findBaseElementName(String completeBaseXPath) {
    String[] xpathParts = completeBaseXPath.split("/");
    return xpathParts[xpathParts.length - 1];
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (participantType == ParticipantType.PARTICIPANT
        && ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), baseElementName, "classCode")) {
      roleType = readRoleType(xmlElement);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "time", "value")) {
      time = ConvertXmlUtil.getDateFromyyyyMMddhhmmss(xmlElement.getAttributeValue("value"));
    }
  }

  protected ROLE_TYPE readRoleType(XMLElement xmlElement) {
    try {
      return ROLE_TYPE.valueOf(xmlElement.getAttributeValue("classCode").toUpperCase());
    } catch (Exception ex) {
      logger.warn("Could not recognize role type [" + xmlElement.getAttributeValue("classCode")
          + "]. Default to caregiver");
      return ROLE_TYPE.CAREGIVER;
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), baseElementName)) {
      participants.add(new ParticipantBuilder()
          .setRoleType(roleType)
          .setPersonIdentity(personHandler.getPerson())
          .setOrganizationIdentity(organizationHandler.getOrganization())
          .setTime(time)
          .setAddress(addressHandler.getAddress())
          .setId(idHandler.getId())
          .setTelecomList(telecomHandler.getTelecoms())
          .build());
      localClear();
    }
  }

  private void localClear() {
    this.idHandler.clear();
    this.addressHandler.clear();
    this.telecomHandler.clear();
    this.time = null;
    this.personHandler.clear();
    this.organizationHandler.clear();
  }

  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    addressHandler.addHandlerToMap(xmlMapping);
    telecomHandler.addHandlerToMap(xmlMapping);
    personHandler.addHandlerToMap(xmlMapping);
    organizationHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void addDataToDocument(ClinicalDocument clinicalDocument) {
    switch (participantType) {
    case AUTHOR:
      clinicalDocument.setAuthor(getSingleParticipant());
      break;
    case LEGAL_AUTHENTICATOR:
      clinicalDocument.setLegalAuthenticator(getSingleParticipant());
      break;
    case DATAENTERER:
      clinicalDocument.setDataEnterer(getSingleParticipant());
      break;
    case INFORMATION_RECIPIENT:
      clinicalDocument.setInformationRecipients(participants);
      break;
    case PARTICIPANT:
      clinicalDocument.setParticipants(participants);
      break;
    default:
      logger.error("Unknown participant type: " + participantType.name());
      break;
    }
  }

  private Participant getSingleParticipant() {
    if (participants.isEmpty()) {
      return null;
    }
    return participants.get(0);
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  public List<Participant> getParticipants() {
    return participants;
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    addressHandler.removeHandlerFromMap(xmlMapping);
    telecomHandler.removeHandlerFromMap(xmlMapping);
    personHandler.removeHandlerFromMap(xmlMapping);
    organizationHandler.removeHandlerFromMap(xmlMapping);
  }
}
