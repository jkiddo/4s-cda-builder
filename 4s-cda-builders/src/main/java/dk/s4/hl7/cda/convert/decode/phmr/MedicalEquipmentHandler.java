package dk.s4.hl7.cda.convert.decode.phmr;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment.MedicalEquipmentBuilder;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class MedicalEquipmentHandler implements XmlHandler {
  private static final String SECTION_TEXT_XPATH = "/ClinicalDocument/component/structuredBody/component/section/text";
  private static final String PLAYING_DEVICE_XPATH = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/participant/participantRole/playingDevice";
  private List<MedicalEquipment> medicalEquipments;
  private MedicalEquipmentBuilder medicalEquipmentBuilder;
  private RawTextHandler rawTextHandler;

  public MedicalEquipmentHandler() {
    this.medicalEquipments = new ArrayList<MedicalEquipment>();
    this.rawTextHandler = new RawTextHandler(SECTION_TEXT_XPATH, "text");
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "playingDevice")) {
      medicalEquipmentBuilder = new MedicalEquipment.MedicalEquipmentBuilder();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "translation", "code")) {
      medicalEquipmentBuilder.setMedicalDeviceCode(xmlElement.getAttributeValue("code"));
      medicalEquipmentBuilder.setMedicalDeviceDisplayName(xmlElement.getAttributeValue("displayName"));
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "manufacturerModelName")) {
      medicalEquipmentBuilder.setManufacturerModelName(xmlElement.getElementValue());
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "softwareName")) {
      medicalEquipmentBuilder.setSoftwareName(xmlElement.getElementValue());
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equals("playingDevice")) {
      medicalEquipments.add(medicalEquipmentBuilder.build());
      medicalEquipmentBuilder = null;
    }
  }

  public void addDataToDocument(PHMRDocument phmrDocument) {
    if (!medicalEquipments.isEmpty()) {
      phmrDocument.setMedicalEquipmentsText(rawTextHandler.getRawText());
      for (MedicalEquipment medicalEquipmentBuild : medicalEquipments) {
        phmrDocument.addMedicalEquipment(medicalEquipmentBuild);
      }
    }
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    rawTextHandler.addHandlerToMap(xmlMapping);
    xmlMapping.add(PLAYING_DEVICE_XPATH, this);
    xmlMapping.add(PLAYING_DEVICE_XPATH + "/code/translation", this);
    xmlMapping.add(PLAYING_DEVICE_XPATH + "/manufacturerModelName", this);
    xmlMapping.add(PLAYING_DEVICE_XPATH + "/softwareName", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(PLAYING_DEVICE_XPATH);
    xmlMapping
        .remove("/ClinicalDocument/component/structuredBody/component/section/entry/organizer/participant/participantRole/playingDevice/code/translation");
    xmlMapping
        .remove("/ClinicalDocument/component/structuredBody/component/section/entry/organizer/participant/participantRole/playingDevice/manufacturerModelName");
    xmlMapping
        .remove("/ClinicalDocument/component/structuredBody/component/section/entry/organizer/participant/participantRole/playingDevice/softwareName");
  }
}