package dk.s4.hl7.cda.convert.decode.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<QFDDDocument> {
  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  private String title;
  private String language;
  private List<Section<QFDDQuestion>> sections;
  private ObservationHandler observationHandler;
  private RawTextHandler rawTextHandler;
  private CopyRightHandler copyRightHandler;
  private SectionInformation copyRight;

  public SectionHandler() {
    observationHandler = new ObservationHandler();
    copyRightHandler = new CopyRightHandler();
    sections = new ArrayList<Section<QFDDQuestion>>();
    rawTextHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
    copyRight = null;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "languageCode", "code")) {
      language = xmlElement.getAttributeValue("code");
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "section")) {
      if (copyRightHandler.isCopyRightSection()) {
        copyRight = new SectionInformation(title, copyRightHandler.getCopyRightText(), language);
      } else {
        Section<QFDDQuestion> section = new Section<QFDDQuestion>(title, rawTextHandler.getRawText(), language);
        for (QFDDQuestion question : observationHandler.getQuestions()) {
          section.addQuestionnaireEntity(question);
        }
        sections.add(section);
      }
      clear();
    }
  }

  public void clear() {
    rawTextHandler.clear();
    title = null;
    language = null;
    observationHandler.clear();
    copyRightHandler.clear();
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    observationHandler.addHandlerToMap(xmlMapping);
    rawTextHandler.addHandlerToMap(xmlMapping);
    copyRightHandler.addHandlerToMap(xmlMapping);
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/languageCode", this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    observationHandler.removeHandlerFromMap(xmlMapping);
    rawTextHandler.removeHandlerFromMap(xmlMapping);
    copyRightHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/languageCode");
  }

  @Override
  public void addDataToDocument(QFDDDocument clinicalDocument) {
    clinicalDocument.setCopyRight(copyRight);
    for (Section<QFDDQuestion> section : sections) {
      clinicalDocument.addSection(section);
    }
  }
}
