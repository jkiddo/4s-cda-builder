package dk.s4.hl7.cda.model;

public class BuilderException extends RuntimeException {
  private static final long serialVersionUID = 1L;
  
  public BuilderException(String message) {
    super(message);
  }
}
