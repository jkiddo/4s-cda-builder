package dk.s4.hl7.cda.model;

public final class Comment {
  private Participant author;
  private String text;

  /** Comment class constructor.
   * 
   * @param author The author person identity.
   * @param organization The organization identity.
   * @param time The time for the comment.
   * @param text The comment text.
   */
  public Comment(Participant author, String text) {
    this.author = author;
    this.text = text;
  }

  /** Get author person identity.
   * @return author person identity.
   */
  public Participant getAuthor() {
    return author;
  }

  /** Get comment text. 
   * @return comment text.
   */
  public String getText() {
    return text;
  }
}
