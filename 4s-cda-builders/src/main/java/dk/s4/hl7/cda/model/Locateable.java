package dk.s4.hl7.cda.model;

/** Define the role of being 'locatable' for an entity,
 * that is, the properties of having an address and a telecommunication
 * connection(s).
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public interface Locateable {

  /** Get the list of telecom.
   * @return The list of telcom.
   */
  Telecom[] getTelecomList();

  /** Get the address data.
   * @return The address data.
   */
  AddressData getAddress();
}
