package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import dk.s4.hl7.cda.codes.NullFlavor;
import dk.s4.hl7.cda.model.AddressData.Use;

public class Patient extends PersonIdentity {
  /** Gender enumeration. */
  public enum Gender {
    /** Female gender. */
    Female,
    /** Male gender. */
    Male,
    /** Undifferentiated gender. */
    Undifferentiated
  }

  private Gender gender;
  private Date birthTime;
  private String ssn;
  private AddressData address;
  private Telecom[] telecomArray;

  public Patient(PatientBuilder builder) {
    super(builder.familyName, builder.givenNames, builder.prefix);
    this.ssn = builder.ssn;
    this.gender = builder.gender;
    this.birthTime = builder.birthTime;
    this.address = builder.address;
    this.telecomArray = builder.telecom;
  }

  public Gender getGender() {
    return gender;
  }

  public Date getBirthTime() {
    return birthTime;
  }

  public String getSSN() {
    return ssn;
  }

  public AddressData getAddress() {
    return address;
  }

  public Telecom[] getTelecomList() {
    return telecomArray;
  }

  public String toString() {
    String result = "PersonIdentity: ";
    // Clumsy logic, but "when null flavor is null, then patient data exists"
    if (nullFlavor == null) {

      result += "G: " + getGender() + " ";

      result += "ID: " + getSSN() + " ";

      result += "(" + getFamilyName() + ",[";
      for (String firstname : getGivenNames()) {
        result += firstname + "/";
      }
      result += "] ";

      result += "Prefix: " + prefix + " ";

      result += "Birth: " + getBirthTime() + " ";

      if (address != null) {
        result += "Adr: " + address.toString() + " ";
      } else {
        result += "Adr: (null)";
      }

      if (telecomArray != null) {
        result += "Telecom: [";
        for (Telecom tc : getTelecomList()) {
          result += tc + ",";
        }
        result += "] ";
      }

      result += ")";
    } else {
      result += "NullFlavor: " + nullFlavor + "\n";
    }
    return result;
  }

  public static class PatientBuilder extends PersonBuilder {
    // Optional parameters
    private String ssn = null;
    private Gender gender = Gender.Undifferentiated;
    private Date birthTime = null;
    private Telecom[] telecom = null;
    private String prefix = null;

    // temporaries
    private int yearAllDigits = 0;
    private int dayInMonth = 0;
    private int month = 0;
    private List<Telecom> teleComListTemporary;
    private AddressData address;

    public PatientBuilder() {
      teleComListTemporary = new ArrayList<Telecom>();
    }

    public PatientBuilder(String familyName) {
      this.familyName = familyName;
      teleComListTemporary = new ArrayList<Telecom>();
    }

    public PatientBuilder setPersonID(String patientId) {
      this.ssn = patientId;
      return this;
    }

    public PatientBuilder setGender(Gender gender) {
      this.gender = gender;
      return this;
    }

    public PatientBuilder setBirthTime(int yearAllDigits, int monthUsingCalendarEnums, int dayInMonth) {
      this.yearAllDigits = yearAllDigits;
      this.month = monthUsingCalendarEnums;
      this.dayInMonth = dayInMonth;
      return this;
    }

    public PatientBuilder setBirthTime(Date birthTime) {
      if (birthTime == null) {
        return this;
      }
      Calendar calendarTime = Calendar.getInstance();
      calendarTime.setTime(birthTime);
      return this.setBirthTime(calendarTime.get(Calendar.YEAR), calendarTime.get(Calendar.MONTH),
          calendarTime.get(Calendar.DAY_OF_MONTH));
    }

    public PatientBuilder addTelecom(Use use, String protocol, String telecomString) {
      teleComListTemporary.add(new Telecom(use, protocol, telecomString));
      return this;
    }

    public PatientBuilder setAddress(AddressData address) {
      this.address = address;
      return this;
    }

    public PatientBuilder addFamilyName(String familyName) {
      this.familyName = familyName;
      return this;
    }

    public PatientBuilder addGivenName(String name) {
      givenNamesTemporary.add(name);
      return this;
    }

    public PatientBuilder setPrefix(String prefix) {
      this.prefix = prefix;
      return this;
    }

    // This is an alternative to the rest of the setters */
    public PatientBuilder noInformation() {
      this.nullFlavor = NullFlavor.NO_INFORMATION;
      return this;
    }

    public Patient build() {
      processGivenNames();
      if (teleComListTemporary.size() > 0) {
        telecom = new Telecom[teleComListTemporary.size()];
        teleComListTemporary.toArray(telecom);
      }

      if (yearAllDigits != 0) {
        Calendar utcCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        utcCalendar.set(yearAllDigits, month, dayInMonth, 0, 0, 0);
        birthTime = utcCalendar.getTime();
      }
      return new Patient(this);
    }

    public PatientBuilder addTelecoms(List<Telecom> telecoms) {
      teleComListTemporary.addAll(telecoms);
      return this;
    }
  }
}
