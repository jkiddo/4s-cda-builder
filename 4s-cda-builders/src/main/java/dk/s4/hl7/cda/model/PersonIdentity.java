package dk.s4.hl7.cda.model;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.codes.NullFlavor;

/**
 * Immutable object to contain information about a person.
 *
 * Maps to POCD_MT000040.Person
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 */
public class PersonIdentity {
  protected String prefix;
  protected String[] givenNames;
  protected String familyName;

  // If nullFlavor is not null, then
  // this information takes precedence over
  // all other fields which are probably null.
  protected NullFlavor nullFlavor;

  /**
   * "Effective Java" builder for constructing person identities.
   *
   * @author Henrik Baerbak Christensen, Aarhus University
   *
   */
  public static class PersonBuilder {
    // Required parameters
    protected String prefix = null;
    protected String familyName;
    protected String[] givenNames;
    
    protected NullFlavor nullFlavor = null;

    // temporaries
    protected List<String> givenNamesTemporary;

    public PersonBuilder() {
      givenNamesTemporary = new ArrayList<String>();
    }

    public PersonBuilder(String familyName) {
      givenNamesTemporary = new ArrayList<String>();
      this.familyName = familyName;
    }

    public PersonBuilder addFamilyName(String familyName) {
      this.familyName = familyName;
      return this;
    }

    public PersonBuilder addGivenName(String name) {
      givenNamesTemporary.add(name);
      return this;
    }

    public PersonBuilder setPrefix(String prefix) {
      this.prefix = prefix;
      return this;
    }

    // This is an alternative to the rest of the setters */
    public PersonBuilder noInformation() {
      this.nullFlavor = NullFlavor.NO_INFORMATION;
      return this;
    }

    public PersonIdentity build() {
      processGivenNames();

      PersonIdentity pi = new PersonIdentity(this);
      return pi;
    }

    protected void processGivenNames() {
      givenNames = new String[givenNamesTemporary.size()];
      givenNamesTemporary.toArray(givenNames);
    }
  }

  private PersonIdentity(PersonBuilder builder) {
    // Clumsy logic, but "when null flavor is null, then patient data exists"
    if (builder.nullFlavor == null) {
      familyName = builder.familyName;
      givenNames = builder.givenNames;
      prefix = builder.prefix;

      nullFlavor = null;
    } else {
      familyName = null;
      givenNames = null;
      prefix = null;
      nullFlavor = builder.nullFlavor;
    }
  }
  
  protected PersonIdentity(String familyName, String[] givenNames, String prefix) {
    this.familyName = familyName;
    this.givenNames = givenNames;
    this.prefix = prefix;
  }

  /**
   * Get the social security number.
   * 
   * @return The social security number.
   */

  /**
   * Get whether person has a prefix.
   * 
   * @return Boolean stating whether there is a prefix.
   */
  public boolean hasPrefix() {
    return prefix != null && prefix.length() > 0;
  }

  /**
   * Get person prefix.
   * 
   * @return prefix string.
   */
  public String getPrefix() {
    return prefix;
  }

  /**
   * Get list of given names.
   * 
   * @return list of given names.
   */
  public String[] getGivenNames() {
    return givenNames;
  }

  /**
   * Get family name.
   * 
   * @return family name string.
   */
  public String getFamilyName() {
    return familyName;
  }

  /**
   * Return the null flavor if this person identity represents some kind of null
   * value. If the person identity is known and defined this method returns
   * null.
   * 
   * @return the null flavor or null in case the person is well defined
   */
  public NullFlavor getNullFlavor() {
    return nullFlavor;
  }

  public String toString() {
    String result = "PersonIdentity: ";
    // Clumsy logic, but "when null flavor is null, then patient data exists"
    if (nullFlavor == null) {

      result += "(" + getFamilyName() + ",[";
      for (String firstname : getGivenNames()) {
        result += firstname + "/";
      }
      result += "] ";

      result += "Prefix: " + prefix + " ";

      result += ")";
    } else {
      result += "NullFlavor: " + nullFlavor + "\n";
    }
    return result;
  }

}
