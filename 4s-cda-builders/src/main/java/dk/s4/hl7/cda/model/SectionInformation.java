package dk.s4.hl7.cda.model;

public class SectionInformation {
  private String title;
  private String text;
  private String language;

  public SectionInformation(String title, String text, String language) {
    super();
    this.title = title;
    this.text = text;
    this.language = language;
  }

  public SectionInformation(String title, String text) {
    this(title, text, "da-DK");
  }

  public String getTitle() {
    return title;
  }

  public String getText() {
    return text;
  }

  public String getLanguage() {
    return language;
  }
}
