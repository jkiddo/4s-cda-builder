package dk.s4.hl7.cda.model;

/** Data structure for a single telecom instance.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Duus Christian Hausmann, Silverbullet A/S
 */
public final class Telecom {

  private String value;
  private String protocol;
  private AddressData.Use use;

  /** Telecom constructor.
   * @param use The address use of the telecom.
   * @param value The telecom value.
   */
  public Telecom(AddressData.Use use, String value) {
    this.value = value;
    this.use = use;
  }

  /** Telecom constructor.
   * @param use The address use of the telecom.
   * @param value The telecom value.
   * @param protocol The protocol for the telecom - ie. mailto, tel etc
   */
  public Telecom(AddressData.Use use, String protocol, String value) {
    this.value = value;
    this.protocol = protocol;
    this.use = use;
  }

  /**
   * Returns the protocol of the telecom
   * @return the protocol
   */
  public String getProtocol() {
    return protocol;
  }

  /** Get the telecom value.
  * @return the telecom value.
  */
  public String getValue() {
    return value;
  }

  /** Get the address use.
   * @return the address use.
   */
  public AddressData.Use getAddressUse() {
    return use;
  }

  public String toString() {
    return getAddressUse() + "/" + getProtocol() + ":" + getValue();
  }
}
