package dk.s4.hl7.cda.model.core;

import java.util.Date;
import java.util.List;

import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Patient;

public interface ClinicalDocument {
  String getRealmCode();

  String getTypeIdRoot();

  String getTypeIdExtension();

  String[] getTemplateIds();

  ID getId();

  String getCode();

  String getCodeDisplayName();

  String getTitle();

  void setTitle(String title);

  /**
   * Define when the document was created.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setEffectiveTime(java.util.Date)
   * @param documentCreationTime
   *          The document creation time.
   */
  void setEffectiveTime(Date now);

  /**
   * Define when the document was created.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setEffectiveTime(java.util.Date)
   * @return documentCreationTime The document creation time.
   */
  Date getEffectiveTime();

  /**
   * 
   * @return
   */
  String getConfidentialityCode();

  /**
   * 
   * @param code
   */
  void setConfidentialityCode(String code);

  /**
   * 
   * @return
   */
  String getLanguageCode();

  /**
   * 
   * @param language
   */
  void setLanguageCode(String language);

  /**
   * Define a unique identifier (CDA Release 2, §4.2.1.7 ClinicalDocument.setId
   * / "Represents an identifier that is common across all document revisions")
   * and the version number (CDA Release 2, §4.2.1.8
   * ClinicalDocument.versionNumber /
   * "An integer value used to version successive replacement documents".
   * Normally for PHMR documents just use a unique identifier, and version
   * number 1.
   * 
   * @param uniqueId
   *          the identifier for the document
   * @param versionNumber
   *          the number of this version
   */
  void setDocumentVersion(String setId, Integer versionNumber);

  /**
   * Gets the unique identifier (CDA Release 2, §4.2.1.7 ClinicalDocument.setId)
   * 
   * @return uniqueId the identifier for the document
   */
  String getUniqueId();

  /**
   * Gets version number (CDA Release 2, §4.2.1.8
   * ClinicalDocument.versionNumber)
   * 
   * @return versionNumber the number of this version
   */
  Integer getVersionNumber();

  /**
   * Define who is the patient.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setPatientRole(org.net4care.phmr.test.model.PersonIdentity)
   * @param patientIdentity
   *          The patient identity as a PersonIdentity.
   * 
   */
  void setPatient(Patient patientIdentity);

  /**
   * Returns the patient.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setPatientRole(org.net4care.phmr.test.model.PersonIdentity)
   * @return patientIdentity The patient identity as a PersonIdentity.
   * 
   */
  Patient getPatient();

  /**
   * Define who is the author of the document, i.e. the organization and person
   * and time when the author participated in creating the document.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setAuthor(org.net4care.phmr.test.model.PersonIdentity,
   *      java.util.Date)
   * @param authorOrganizationIdentity
   *          The author organization identity.
   * @param authorPersonIdentity
   *          The author person identity.
   * @param authorParticipationStart
   *          The author participation start.
   */
  void setAuthor(Participant Author);

  Participant getAuthor();

  void setDataEnterer(Participant dataEnterer);

  Participant getDataEnterer();

  /**
   * Set the organization that is custodian of the document.
   * 
   * @param custodianIdentity
   *          The custodian organization identity.
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setCustodian(org.net4care.phmr.test.model.OrganizationIdentity)
   */
  void setCustodian(OrganizationIdentity custodianIdentity);

  OrganizationIdentity getCustodianIdentity();

  void setLegalAuthenticator(Participant legalAuthenticator);

  Participant getLegalAuthenticator();

  /**
   * Set the time interval that the measurements span.
   * 
   * @see org.BaseClinicalDocument.phmr.test.ClinicalDocument#setDocumentationTime(java.util.Date,
   *      java.util.Date)
   * @param from
   *          The earliest time in the time interval.
   * @param to
   *          The latest time in the time interval.
   */
  void setDocumentationTimeInterval(Date from, Date to);

  /**
   * Get the time start for the measurements span.
   * 
   * @return from The earliest time in the time interval.
   */
  Date getServiceStartTime();

  /**
   * Get the end time for the measurements span.
   * 
   * @return from The latest time in the time interval.
   */
  Date getServiceStopTime();

  List<Participant> getParticipants();

  void setParticipants(List<Participant> participants);

  void addParticipant(Participant participant);

  List<Participant> getInformationRecipients();

  void setInformationRecipients(List<Participant> participants);

  void addInformationRecipients(Participant participant);

  String getSetId();

  void setSetId(String setId);
}