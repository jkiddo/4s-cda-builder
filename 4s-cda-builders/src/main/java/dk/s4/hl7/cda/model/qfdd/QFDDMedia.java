package dk.s4.hl7.cda.model.qfdd;

// Not supported by builder
@Deprecated
public class QFDDMedia {
  private final String mediaType;
  private final String representation;
  private final String referenceValue;

  private QFDDMedia(QFDDMediaBuilder builder) {
    this.mediaType = builder.mediaType;
    this.representation = builder.representation;
    this.referenceValue = builder.referenceValue;
  }

  //  Not supported by builder
  @Deprecated
  public static class QFDDMediaBuilder {
    private String mediaType;
    private String representation;
    private String referenceValue;

    public QFDDMediaBuilder mediaType(String mediaType1) {
      this.mediaType = mediaType1;
      return this;
    }

    public QFDDMediaBuilder representation(String representation) {
      this.representation = representation;
      return this;
    }

    public QFDDMediaBuilder referenceValue(String referenceValue) {
      this.referenceValue = referenceValue;
      return this;
    }

    public QFDDMedia build() {
      return new QFDDMedia(this);
    }
  }

  public String getMediaType() {
    return mediaType;
  }

  public String getRepresentation() {
    return representation;
  }

  public String getReferenceValue() {
    return referenceValue;
  }

}
