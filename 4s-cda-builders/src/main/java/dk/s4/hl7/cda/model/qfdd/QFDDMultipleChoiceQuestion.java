package dk.s4.hl7.cda.model.qfdd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;

public class QFDDMultipleChoiceQuestion extends QFDDQuestion {

  private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();
  private String minimum; // low
  private String maximum; // high

  /**
   * "Effective Java" Builder for constructing QFDDNumericResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */

  public static class QFDDMultipleChoiceQuestionBuilder
      extends QFDDQuestion.BaseQFDDQuestionBuilder<QFDDMultipleChoiceQuestion, QFDDMultipleChoiceQuestionBuilder> {
    public QFDDMultipleChoiceQuestionBuilder() {
    }

    private List<CodedValue> answerOptionList = new ArrayList<CodedValue>();
    private String minimum; // low
    private String maximum; // high

    public QFDDMultipleChoiceQuestionBuilder setMinimum(String minimum) {
      this.minimum = minimum;
      return this;
    }

    public QFDDMultipleChoiceQuestionBuilder setMaximum(String maximum) {
      this.maximum = maximum;
      return this;
    }

    public QFDDMultipleChoiceQuestion build() {
      return new QFDDMultipleChoiceQuestion(this);
    }
    
    public void addAnswerOption(String code, String codeSystem, String displayName, String codeSystemName) {
      answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
    }

    @Override
    public QFDDMultipleChoiceQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDMultipleChoiceQuestion(QFDDMultipleChoiceQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    answerOptionList = builder.answerOptionList;
  }

  @Override
  public String toString() {
    return getQuestion();
  }

  public void addAnswerOption(String code, String codeSystem, String displayName, String codeSystemName) {
    answerOptionList.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
  }

  public List<CodedValue> getAnswerOptionList() {
    return answerOptionList;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }
}
