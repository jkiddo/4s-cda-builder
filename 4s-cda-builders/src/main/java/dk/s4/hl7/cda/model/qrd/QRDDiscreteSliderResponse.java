package dk.s4.hl7.cda.model.qrd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.util.ModelUtil;

public final class QRDDiscreteSliderResponse extends QRDResponse {
  private static final Logger logger = LoggerFactory.getLogger(QRDDiscreteSliderResponse.class);
  private CodedValue answer;
  private int minimum;

  /**
   * "Effective Java" Builder for constructing QRDDiscreteSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDDiscreteSliderResponseBuilder extends
      QRDResponse.BaseQRDResponseBuilder<QRDDiscreteSliderResponse, QRDDiscreteSliderResponseBuilder> {

    private CodedValue answer;
    private int minimum;

    public QRDDiscreteSliderResponseBuilder() {
      this.minimum = -1;
    }

    public QRDDiscreteSliderResponseBuilder setMinimum(int minimum) {
      this.minimum = validateMinimumRange(minimum);
      return this;
    }

    private int validateMinimumRange(int minimum) {
      if (minimum != 0 && minimum != 1) {
        logger.warn("The minimum in the Discrete Slider must be 1 or 0 but was: " + minimum);
        logger.warn("Defaulting to 0");
        return 0;
      }
      return minimum;
    }

    public QRDDiscreteSliderResponseBuilder setMinimum(String minimum) {
      ModelUtil.checkNull(minimum, "Minimum is mandatory");
      int minTemp = -1;
      try {
        minTemp = Integer.parseInt(minimum);
      } catch (Exception ex) {
        logger.warn("The minimum in the Discrete Slider must be 1 or 0 but was: " + minimum);
        logger.warn("Defaulting to 0");
        minTemp = 0;
      }
      setMinimum(minTemp);
      return this;
    }

    @Override
    public QRDDiscreteSliderResponse build() {
      validateMinimumRange(minimum);
      if (minimum == 1 && answer == null) {
        throw new CdaBuilderException("An answer is required but no answer is given");
      }
      return new QRDDiscreteSliderResponse(this);
    }

    @Override
    public QRDDiscreteSliderResponseBuilder getThis() {
      return this;
    }

    public QRDDiscreteSliderResponseBuilder setAnswer(String code, String codeSystem, String displayName,
        String codeSystemName) {
      answer = new CodedValue(code, codeSystem, displayName, codeSystemName);
      return this;
    }
  }

  private QRDDiscreteSliderResponse(QRDDiscreteSliderResponseBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    answer = builder.answer;
  }

  public void setAnswer(String code, String codeSystem, String displayName, String codeSystemName) {
    answer = new CodedValue(code, codeSystem, displayName, codeSystemName);
  }

  public CodedValue getAnswer() {
    return answer;
  }

  public int getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return "1";
  }
}
