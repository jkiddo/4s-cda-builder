package dk.s4.hl7.cda.model.qrd;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.model.CodedValue;

public class QRDMultipleChoiceResponse extends QRDResponse {

  private List<CodedValue> answers = new ArrayList<CodedValue>();
  private String minimum; // low
  private String maximum; // high

  /**
   * "Effective Java" Builder for constructing QRDNumericResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDMultipleChoiceResponseBuilder
      extends QRDResponse.BaseQRDResponseBuilder<QRDMultipleChoiceResponse, QRDMultipleChoiceResponseBuilder> {
    public QRDMultipleChoiceResponseBuilder() {
    }

    private List<CodedValue> answers = new ArrayList<CodedValue>();
    private String minimum; // low
    private String maximum; // high

    public QRDMultipleChoiceResponseBuilder setMinimum(String minimum) {
      this.minimum = minimum;
      return this;
    }

    public QRDMultipleChoiceResponseBuilder setMaximum(String maximum) {
      this.maximum = maximum;
      return this;
    }
    
    public QRDMultipleChoiceResponseBuilder addAnswer(String code, String codeSystem, String displayName, String codeSystemName) {
      answers.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
      return this;
    }

    public QRDMultipleChoiceResponse build() {
      return new QRDMultipleChoiceResponse(this);
    }

    @Override
    public QRDMultipleChoiceResponseBuilder getThis() {
      return this;
    }
  }

  private QRDMultipleChoiceResponse(QRDMultipleChoiceResponseBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    answers = builder.answers;
  }

  @Override
  public String toString() {
    return getQuestion();
  }

  public void addAnswer(String code, String codeSystem, String displayName, String codeSystemName) {
    answers.add(new CodedValue(code, codeSystem, displayName, codeSystemName));
  }
  
  public List<CodedValue> getAnswers() {
    return answers;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }
}
