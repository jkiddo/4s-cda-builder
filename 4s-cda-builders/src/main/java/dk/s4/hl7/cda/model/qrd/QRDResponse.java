package dk.s4.hl7.cda.model.qrd;

import dk.s4.hl7.cda.model.QuestionnaireEntity;

public abstract class QRDResponse extends QuestionnaireEntity {
  protected QRDResponse(QuestionnaireEntityBuilder<?, ?> builder) {
    super(builder);
  }

  public static abstract class BaseQRDResponseBuilder<R extends QRDResponse, T extends QuestionnaireEntityBuilder<R, T>>
      extends QuestionnaireEntityBuilder<R, T> {
    
    public BaseQRDResponseBuilder() {
    }

    public abstract T getThis();
   
    public abstract R build();
  }
}
