package dk.s4.hl7.util.xml;

import java.util.HashMap;
import java.util.Map;

public class XmlMapping {
  private Map<String, XmlHandler> handlerMap;

  public XmlMapping() {
    this.handlerMap = new HashMap<String, XmlHandler>();
  }

  public void add(String xpath, XmlHandler xmlHandler) {
    xpath = xpath.toLowerCase();
    if (handlerMap.containsKey(xpath)) {
      System.out.println("Key already exists: " + xpath);
    }
    handlerMap.put(xpath, xmlHandler);
  }

  public void remove(String xpath) {
    handlerMap.remove(xpath.toLowerCase());
  }

  public XmlHandler get(String xpath) {
    return handlerMap.get(xpath.toLowerCase());
  }
}