/**
 * The MIT License
 *
 * Original work sponsored and donated by National Board of e-Health (NSI), Denmark
 * (http://www.nsi.dk)
 *
 * Copyright (C) 2016 National Board of e-Health (NSI), Denmark (http://www.nsi.dk)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package dk.s4.hl7.util.xml;

import java.io.IOException;
import java.util.Stack;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * Very simple xml builder with no error checking
 * 
 * This class is not responsible for flushing or closing the outputStream
 */
public class XmlStreamBuilder {
  private static final int EXPECTED_MAX_XML_DEPTH = 20;
  private Stack<String> elementNameStack;
  private Appendable outputStream;
  private boolean endTagMissing; // '>' is missing
  private boolean previousElementEnded; // previous element is an </...> tag
  private XmlPrettyPrinter xmlPrettyPrinter;

  public XmlStreamBuilder(Appendable outputStream) {
    this(outputStream, null);
  }

  public XmlStreamBuilder(Appendable outputStream, XmlPrettyPrinter xmlPrettyPrinter) {
    elementNameStack = new Stack<String>();
    elementNameStack.ensureCapacity(EXPECTED_MAX_XML_DEPTH);
    endTagMissing = false;
    previousElementEnded = false;
    this.outputStream = outputStream;
    this.xmlPrettyPrinter = xmlPrettyPrinter;
  }

  public XmlStreamBuilder element(String name) throws IOException {
    checkNull(name, "The element name is missing");
    if (hasParent()) {
      outputStream.append('>');
      newLineIfPrettyPrint();
    }
    indentIfPrettyPrinter(elementNameStack.size());
    elementNameStack.push(name);
    outputStream.append('<').append(name);
    endTagMissing = true;
    previousElementEnded = false;
    return this;
  }

  public XmlStreamBuilder elementEnd() throws IOException {
    if (endTagMissing) {
      outputStream.append(">");
      newLineIfPrettyPrint();
      indentIfPrettyPrinter(elementNameStack.size());
    } else if (previousElementEnded) {
      indentIfPrettyPrinter(elementNameStack.size() - 1);
      outputStream.append("</").append(elementNameStack.pop()).append('>');
    } else {
      outputStream.append("</").append(elementNameStack.pop()).append('>');
    }
    newLineIfPrettyPrint();
    endTagMissing = false;
    previousElementEnded = true;
    return this;
  }
  
  private static void checkNull(String text, String message) {
    if (text == null || text.trim().equalsIgnoreCase("null")) {
      throw new IllegalArgumentException(message);
    }
  }

  public XmlStreamBuilder elementShortEnd() throws IOException {
    outputStream.append("/>");
    newLineIfPrettyPrint();
    elementNameStack.pop();
    endTagMissing = false;
    previousElementEnded = true;
    return this;
  }

  public XmlStreamBuilder attribute(String name, String value) throws IOException {
    checkNull(name, "The attribute name is missing");
    checkNull(value, "The attribute value is missing for attribute: " + name);
    outputStream.append(' ').append(name).append("=\"").append(StringEscapeUtils.escapeXml11(value)).append("\"");
    endTagMissing = true;
    previousElementEnded = false;
    return this;
  }

  public XmlStreamBuilder attribute(String name, char value) throws IOException {
    checkNull(name, "The attribute name is missing");
    // No check for value because the type char cannot be null
    outputStream.append(' ').append(name).append("=\"").append(StringEscapeUtils.escapeXml11(value + "")).append("\"");
    endTagMissing = true;
    previousElementEnded = false;
    return this;
  }

  public XmlStreamBuilder value(String value) throws IOException {
    checkNull(value, "The value for the element [" + elementNameStack.peek() + "] is missing");
    outputStream.append('>').append(StringEscapeUtils.escapeXml11(value));
    endTagMissing = false;
    previousElementEnded = false;
    return this;
  }

  public XmlStreamBuilder valueNoEscaping(String value) throws IOException {
    checkNull(value, "The value for the element [" + elementNameStack.peek() + "] is missing");
    outputStream.append('>').append(value);
    endTagMissing = false;
    previousElementEnded = false;
    return this;
  }

  private boolean hasParent() {
    return endTagMissing && !elementNameStack.isEmpty();
  }

  private void newLineIfPrettyPrint() throws IOException {
    if (xmlPrettyPrinter != null) {
      xmlPrettyPrinter.appendNewline(outputStream);
    }
  }

  private void indentIfPrettyPrinter(int depth) throws IOException {
    if (xmlPrettyPrinter != null) {
      xmlPrettyPrinter.appendIndentation(outputStream, depth);
    }
  }

  /**
   *  
   * Usage helps IDE autoformatter to provide correct indentation of chained method
   * call. The indentation follows the normal XML pretty print indentation.
   * 
   * @param simpleXmlStreamBuilder
   * @return
   */
  public XmlStreamBuilder addElement(XmlStreamBuilder simpleXmlStreamBuilder) {
    return this;
  }

  public void addDefaultPI() throws IOException {
    outputStream.append("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
    newLineIfPrettyPrint();
  }

  public void addCustomPI(String pi) throws IOException {
    outputStream.append(pi);
    newLineIfPrettyPrint();
  }
}
