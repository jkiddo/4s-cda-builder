package dk.s4.hl7.cda.convert;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.SetupQFDDAnalogSliderExample;
import dk.s4.hl7.cda.model.qfdd.SetupQFDDDiscreteSliderExample;
import dk.s4.hl7.cda.model.qfdd.SetupQFDDMultibleChoiseExample;
import dk.s4.hl7.cda.model.qfdd.SetupQFDDNumericExample;
import dk.s4.hl7.cda.model.qfdd.SetupQFDDTextExample;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

public class TestXmlToQfdd {
  private QFDDXmlCodec codec = new QFDDXmlCodec();
  private QFDDDocument cda;

  @Test
  public void testQFDDComplete() throws Exception {
    cda = SetupQFDDAnalogSliderExample.defineAsCDA();
    addSectionsToDocument(cda, SetupQFDDDiscreteSliderExample.defineAsCDA().getSections());
    addSectionsToDocument(cda, SetupQFDDMultibleChoiseExample.defineAsCDA().getSections());
    addSectionsToDocument(cda, SetupQFDDNumericExample.defineAsCDA().getSections());
    addSectionsToDocument(cda, SetupQFDDTextExample.defineAsCDA().getSections());
    cda.getSections().add(0, new Section<QFDDQuestion>(new SectionInformation("introduction", "introduction")));
    cda.setCopyRight(new SectionInformation("copyright", "copyright"));
    cda.addSection(new Section<QFDDQuestion>(new SectionInformation("final comment", "final comment")));

    String expectedEncodedXml = null;
    QFDDDocument expectedDecoded = null;
    String expectedEncodedNew = null;

    try {
      expectedEncodedXml = encode(cda);
      byte ptext[] = expectedEncodedXml.getBytes();
      String value = new String(ptext);
      expectedDecoded = decode(value);
      expectedEncodedNew = encode(expectedDecoded);
      compare(expectedEncodedXml, expectedEncodedNew);
    } catch (Exception e) {
      outXml(expectedEncodedXml, expectedEncodedNew, new StringBuffer("Kan ikke parse XML, det er ikke valid"));
      assertFalse("Kan ikke parse XML, det er ikke valid", true);
    }
  }

  private void addSectionsToDocument(QFDDDocument cda2, List<Section<QFDDQuestion>> sections) {
    for (Section<QFDDQuestion> section : sections) {
      cda2.addSection(section);
    }
  }

  private QFDDDocument decode(String qrdAsXML) {
    long start = System.currentTimeMillis();
    QFDDDocument document = codec.decode(qrdAsXML);
    long end = System.currentTimeMillis();
    System.out.println("Duration: " + (end - start));
    return document;
  }

  private String encode(QFDDDocument qfddDocument) {
    return codec.encode(qfddDocument);
  }

  public void compare(String expectedXml, String computedXml) throws SAXException, IOException {
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expectedXml, computedXml);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));
    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      outXml(expectedXml, computedXml, detailedMessage);
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private void outXml(String expectedXml, String computedXml, StringBuffer detailedMessage) {
    System.out.println("------------- Detailed Message --------------");
    System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
    System.out.println("------------- Expected XML ------------------");
    System.out.print(HelperMethods.indentLines(expectedXml));
    System.out.println("------------- Computed XML ------------------");
    System.out.print(HelperMethods.indentLines(computedXml));
    System.out.println("------------- End of details ----------------");
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }

}
