package dk.s4.hl7.cda.medcom.examples;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qfdd.QFDDDocument;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

public class QFDDMajorDepressionInventory {
  @Test
  public void createQfdd() {
    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as author
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension("e980c9b4-feba-400f-8e8c-6970778e618d")
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QFDDDocument document = new QFDDDocument(id);
    document.setTitle("Major Depression Inventory (MDI) spørgeskema");

    // 1.1 Populate with time and version info
    document.setEffectiveTime(documentCreationTime);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    document.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    document.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Section<QFDDQuestion> qfddSection = new Section<QFDDQuestion>("Major Depression Inventory (MDI) Test",
        "De følgende spørgsmål handler om, hvordan du har haft det gennem de sidste to uger.");

    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("1",
        "Hvor stor en del af tiden har du følt dig trist til mode, ked af det?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("2",
        "Hvor stor en del af tiden har du mistet interessen for dine daglige gøremål?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("3",
        "Hvor stor en del af tiden har du mistet energien og kræfterne?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("4",
        "Hvor stor en del af tiden har du haft mindre selvtillid?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("5",
        "Hvor stor en del af tiden har du haft dårlig samvittighed eller skyldfølelse?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("6",
        "Hvor stor en del af tiden har du følt, at livet ikke er værd at leve?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("7",
            "Hvor stor en del af tiden har du haft besvær med at koncentrere dig, f.eks. at læse avis eller følge med i fjernsyn?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("8a",
        "Hvor stor en del af tiden har du følt dig rastløs?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("8b",
        "Hvor stor en del af tiden har du været mere stille?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("9",
        "Hvor stor en del af tiden har du haft besvær med at sove om natten?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("10a",
        "Hvor stor en del af tiden har du haft nedsat appetit?"));
    qfddSection.addQuestionnaireEntity(createMultipleChoiceQuestion("10b",
        "Hvor stor en del af tiden har du haft øget appetit?"));

    document.addSection(qfddSection);
  }

  private QFDDMultipleChoiceQuestion createMultipleChoiceQuestion(String questionCode, String question) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, "displayName",
        MedCom.MEDCOM_PROMPT_TABLE);
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.MEDCOM_PROMPT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QFDDMultipleChoiceQuestion qfddMultipleChoiceQuestion = new QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder()
        .setMinimum("1")
        .setMaximum("1")
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion(question)
        .build();
    qfddMultipleChoiceQuestion
        .addAnswerOption("A1", MedCom.MEDCOM_PROMPT_OID, "Hele tiden", MedCom.MEDCOM_PROMPT_TABLE);
    qfddMultipleChoiceQuestion.addAnswerOption("A2", MedCom.MEDCOM_PROMPT_OID, "Det meste af tiden",
        MedCom.MEDCOM_PROMPT_TABLE);
    qfddMultipleChoiceQuestion.addAnswerOption("A3", MedCom.MEDCOM_PROMPT_OID, "Lidt over halvdelen af tiden",
        MedCom.MEDCOM_PROMPT_TABLE);
    qfddMultipleChoiceQuestion.addAnswerOption("A4", MedCom.MEDCOM_PROMPT_OID, "Lidt under halvdelen af tiden",
        MedCom.MEDCOM_PROMPT_TABLE);
    qfddMultipleChoiceQuestion.addAnswerOption("A5", MedCom.MEDCOM_PROMPT_OID, "Lidt af tiden",
        MedCom.MEDCOM_PROMPT_TABLE);
    qfddMultipleChoiceQuestion.addAnswerOption("A6", MedCom.MEDCOM_PROMPT_OID, "På intet tidspunkt",
        MedCom.MEDCOM_PROMPT_TABLE);
    return qfddMultipleChoiceQuestion;
  }

  private String uuid() {
    return UUID.randomUUID().toString();
  }

}
