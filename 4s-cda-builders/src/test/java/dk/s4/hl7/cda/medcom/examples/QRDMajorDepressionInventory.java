package dk.s4.hl7.cda.medcom.examples;

import java.util.Date;
import java.util.UUID;

import org.junit.Test;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

public class QRDMajorDepressionInventory {
  @Test
  public void createQfdd() {
    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as author
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QRDDocument document = new QRDDocument(id);
    document.setTitle("Major Depression Inventory (MDI) spørgeskema");

    String qfddReferenceUuid = "e980c9b4-feba-400f-8e8c-6970778e618d";

    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    document.setPatient(nancy);

    // 1.1 Populate with time and version info
    document.setEffectiveTime(documentCreationTime);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    document.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    document.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    document.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Section<QRDResponse> section = new Section<QRDResponse>("Major Depression Inventory (MDI) Test",
        "De følgende spørgsmål handler om, hvordan du har haft det gennem de sidste to uger.");
    section.addQuestionnaireEntity(createMultipleChoiceResponse("1",
        "Hvor stor en del af tiden har du følt dig trist til mode, ked af det?", 1, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("2",
        "Hvor stor en del af tiden har du mistet interessen for dine daglige gøremål?", 2, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("3",
        "Hvor stor en del af tiden har du mistet energien og kræfterne?", 3, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("4",
        "Hvor stor en del af tiden har du haft mindre selvtillid?", 4, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("5",
        "Hvor stor en del af tiden har du haft dårlig samvittighed eller skyldfølelse?", 5, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("6",
        "Hvor stor en del af tiden har du følt, at livet ikke er værd at leve?", 6, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("7",
            "Hvor stor en del af tiden har du haft besvær med at koncentrere dig, f.eks. at læse avis eller følge med i fjernsyn?",
            1, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("8a",
        "Hvor stor en del af tiden har du følt dig rastløs?", 2, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("8b",
        "Hvor stor en del af tiden har du været mere stille?", 3, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("9",
        "Hvor stor en del af tiden har du haft besvær med at sove om natten?", 4, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("10a",
        "Hvor stor en del af tiden har du haft nedsat appetit?", 5, qfddReferenceUuid));
    section.addQuestionnaireEntity(createMultipleChoiceResponse("10b",
        "Hvor stor en del af tiden har du haft øget appetit?", 6, qfddReferenceUuid));

    document.addSection(section);
  }

  private QRDMultipleChoiceResponse createMultipleChoiceResponse(String questionCode, String question, int answerIndex,
      String qfddReferenceUuid) {
    CodedValue codeValue = new CodedValue(questionCode, MedCom.MEDCOM_PROMPT_OID, "displayName",
        MedCom.MEDCOM_PROMPT_TABLE);
    ID id = new ID.IDBuilder()
        .setRoot(MedCom.MEDCOM_PROMPT_OID)
        .setExtension(uuid())
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    QRDMultipleChoiceResponse multipleChoiceResponse = new QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder()
        .setMinimum("1")
        .setMaximum("1")
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion(question)
        .build();
    addAnswer(answerIndex, multipleChoiceResponse);
    ID qfddId = new ID.IDBuilder()
        .setRoot(MedCom.ROOT_OID)
        .setExtension(qfddReferenceUuid)
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .build();
    CodedValue qfddCodedValue = new CodedValueBuilder()
        .setCode(Loinc.QFD_CODE)
        .setCodeSystem(Loinc.OID)
        .setDisplayName(Loinc.DISPLAYNAME)
        .build();
    multipleChoiceResponse
        .addReference(new Reference.ReferenceBuilder(Reference.DocumentIdReferencesUse.CDA_DOCUMENT_ID_REFERENCE
            .getReferencesUse(), qfddId, qfddCodedValue).build());
    return multipleChoiceResponse;
  }

  private void addAnswer(int answerIndex, QRDMultipleChoiceResponse multipleChoiceResponse) {
    if (answerIndex == 1) {
      multipleChoiceResponse.addAnswer("A1", MedCom.MEDCOM_PROMPT_OID, "Hele tiden", MedCom.MEDCOM_PROMPT_TABLE);
    } else if (answerIndex == 2) {
      multipleChoiceResponse
          .addAnswer("A2", MedCom.MEDCOM_PROMPT_OID, "Det meste af tiden", MedCom.MEDCOM_PROMPT_TABLE);
    } else if (answerIndex == 3) {
      multipleChoiceResponse.addAnswer("A3", MedCom.MEDCOM_PROMPT_OID, "Lidt over halvdelen af tiden",
          MedCom.MEDCOM_PROMPT_TABLE);
    } else if (answerIndex == 4) {
      multipleChoiceResponse.addAnswer("A4", MedCom.MEDCOM_PROMPT_OID, "Lidt under halvdelen af tiden",
          MedCom.MEDCOM_PROMPT_TABLE);
    } else if (answerIndex == 5) {
      multipleChoiceResponse.addAnswer("A5", MedCom.MEDCOM_PROMPT_OID, "Lidt af tiden", MedCom.MEDCOM_PROMPT_TABLE);
    } else if (answerIndex == 6) {
      multipleChoiceResponse
          .addAnswer("A6", MedCom.MEDCOM_PROMPT_OID, "På intet tidspunkt", MedCom.MEDCOM_PROMPT_TABLE);
    }
  }

  private String uuid() {
    return UUID.randomUUID().toString();
  }
}
