package dk.s4.hl7.cda.model.qfdd;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

/**
 * Helper methods to create Numeric example
 * 
 * @author Frank Jacobsen, Systematic
 * 
 */
public class SetupQFDDNumericExample {

  public static QFDDDocument defineAsCDA() {
    QFDDDocument defineQFDDDocumentHeaderInformation = defineQFDDDocumentHeaderInformation();
    return defineQFDDDocumentHeaderInformation;
  }

  /**
   * 
   * Define a CDA for the Medcom Numeric example
   **/

  public static QFDDDocument defineQFDDDocumentHeaderInformation() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a QFDD document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(UUID.randomUUID().toString())
        .setRoot(MedCom.ROOT_OID)
        .build();
    QFDDDocument qfddDocument = new QFDDDocument(idHeader);
    qfddDocument.setTitle("SPØRGESKEMA 1 OM DIN EPILEPSI");
    qfddDocument.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    qfddDocument.setDocumentVersion("2358344", 4711);
    qfddDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qfddDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qfddDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qfddDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qfddDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qfddDocument.setDocumentationTimeInterval(from, to);

    String text = "OM DETTE SKEMA:  Vi bruger blandt andet dine svar til at vurdere, om du har brug for en konsultation."
        + " Vi vil gerne vide, hvornår du havde dit seneste anfald.";

    // Set introduction section
    qfddDocument.addSection(new Section<QFDDQuestion>("Indledning", text));

    // Set CopyRight section
    qfddDocument.setCopyRight(new SectionInformation("Copyright section", "Copyright tekst skrives her"));

    Section<QFDDQuestion> section = new Section<QFDDQuestion>("Sektion 1", "Some text");

    CodedValue codeValue = new CodedValue("74465-6", "Continua-Q-QID", "Some-Display-Name", "Some-CodeSystem-Name");
    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QFDDQuestion qfddQuestion = new QFDDNumericQuestion.QFDDNumericQuestionBuilder()
        .setInterval("1", "1", IntervalType.IVL_INT)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Skriv årstal - skriv 9-taller hvis du ikke husker dette")
        .build();

    section.addQuestionnaireEntity(qfddQuestion);

    // Media pattern
    // QFDDMedia qfddMedia = new QFDDMedia.QFDDMediaBuilder()
    // .mediaType("text/plain")
    // .representation("B64")
    // .referenceValue("test.jpg")
    // .build();

    // Help Text pattern
    QFDDHelpText qfddHelpText = new QFDDHelpText.QFDDHelpTextBuilder()
        .language("da-DK")
        .helpText("Indtast et tal mellem 0 og 24")
        .build();

    // Feedback pattern
    QFDDFeedback qfddFeedback = new QFDDFeedback.QFDDFeedbackBuilder().feedBackText(
        "Don´t take coffeee just before going to bed").build();
    QFDDPrecondition qfddPreconditionFeedback1 = new QFDDPrecondition.QFDDPreconditionBuilder(codeValue)
        .setMinimum("1")
        .setMaximum("50")
        .setValueType(IntervalType.IVL_INT.name())
        .build();
    QFDDPrecondition qfddPreconditionFeedback2 = new QFDDPrecondition.QFDDPreconditionBuilder(codeValue).setAnswer(
        new CodedValue("A1", "JA")).build();
    qfddFeedback.addQFDDPrecondition(qfddPreconditionFeedback1);
    qfddFeedback.addQFDDPrecondition(qfddPreconditionFeedback2);

    QFDDQuestion qfddQuestion1 = new QFDDNumericQuestion.QFDDNumericQuestionBuilder()
        .setInterval("2", "2", IntervalType.IVL_INT)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("Skriv måned - skriv 9-taller hvis du ikke husker dette")
        // .setQFDDMedia(qfddMedia)
        .setHelpText(qfddHelpText)
        .setFeedback(qfddFeedback)
        .build();

    // Example numric precondition
    QFDDPrecondition qfddPrecondition1 = new QFDDPrecondition.QFDDPreconditionBuilder(codeValue).setAnswer(
        new CodedValue("A1", "JA")).build();

    // Example precondition
    QFDDPrecondition qfddPrecondition2 = new QFDDPrecondition.QFDDPreconditionBuilder(codeValue).setAnswer(
        new CodedValue("A1", "JA")).build();

    qfddQuestion1.addPrecondition(qfddPrecondition1);
    qfddQuestion1.addPrecondition(qfddPrecondition2);

    section.addQuestionnaireEntity(qfddQuestion1);

    qfddDocument.addSection(section);

    // Set FinalComments section
    qfddDocument.addSection(new Section<QFDDQuestion>("Afslutning", "Mange tak for din deltagelse. Med venlig hilsen"));

    return qfddDocument;
  }
}
