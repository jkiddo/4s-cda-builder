package dk.s4.hl7.cda.model.qfdd;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

/**
 * Test QFDD Dsicrete Slider example
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class TestQFDDDiscreteSliderExample extends QFDDBaseTest {

  QFDDDocument cda;

  @Before
  public void setup() throws ParserConfigurationException, SAXException, IOException {

    cda = SetupQFDDDiscreteSliderExample.defineAsCDA();

    // A) Set effective time to 10:11:12 instead
    cda.setEffectiveTime(HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 11, 12));

    // B) Setup 'fake' afdeling X to make a difference in the XML that we can
    // spot
    OrganizationIdentity authenticatorOrgIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus X")
        .build();

    // Setup Anders Andersen as authenticator
    PersonIdentity authenticatorIdentity = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    cda.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(
            new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Hjertemedicinsk afdeling X")
                .addAddressLine("Valdemarsgade 53x")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
        .setSOR("88878685")
        .addTelecom(Use.WorkPlace, "tel", "55555555")
        .setTime(at1000onJan13)
        .setPersonIdentity(authenticatorIdentity)
        .setOrganizationIdentity(authenticatorOrgIdentity)
        .build());

    // use the QFDDXmlConverter to create the XML representation
    asString = createQFDDDFromCDA(cda);

  }

  /* Smoke test the header */
  @Test
  public void shouldValidate() {

    assertNotNull("The qrd document is null.", qrdAsXML);

    assertTrue("QRD has not the proper title", asString.contains("<title>Indledning</title>"));
    assertTrue("QRD has not the DK templateID", asString.contains("<templateId root=\"1.2.208.184.12.1\"/>"));

    String uuid = HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("extension", 0, "id",
        "ClinicalDocument", qrdAsXML);

    try {
      UUID.fromString(uuid);
    } catch (IllegalArgumentException e) {
      Assert.fail("QRD has not the proper id: " + uuid);
    }
    assertTrue("typeId / root is incorrect", asString.contains("root=\"2.16.840.1.113883.1.3\""));

    assertTrue("effectiveTime is missing or incorrectly formatted",
        asString.contains("<effectiveTime value=\"20140113101112+0100\"/>"));

    assertTrue("language code missing or wrong", asString.contains("<languageCode code=\"da-DK\"/>"));

    assertTrue("versionNumber missing or wrong", asString.contains("<versionNumber value=\"1\"/>"));

    assertTrue(HelperMethods.testPOCDMT000040ClinicalDocumentTrue(asString));
  }
}
