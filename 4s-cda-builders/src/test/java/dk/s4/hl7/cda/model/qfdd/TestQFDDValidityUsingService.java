package dk.s4.hl7.cda.model.qfdd;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Date;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import generated.CdaType;
import goimplement.it.ClientBuilder;
import goimplement.it.ValidationResponse;

/**
 * Test QFDD Text example
 *
 * @author Frank Jacobsen, Systematic
 * 
 */
public final class TestQFDDValidityUsingService extends QFDDBaseTest {

  QFDDDocument cda;

  @Before
  public void setup() throws ParserConfigurationException, SAXException, IOException {
    // Step 1. Define the Medcom EX1 CDA
    cda = SetupQFDDTextExample.defineAsCDA();

    // A) Set effective time to 10:11:12 instead
    cda.setEffectiveTime(HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 11, 12));

    // B) Setup 'fake' afdeling X to make a difference in the XML that we can
    // spot
    final OrganizationIdentity authenticatorOrgIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus X")
        .build();

    // Setup Anders Andersen as authenticator
    final PersonIdentity authenticatorIdentity = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    final Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);
    cda.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(
            new AddressData.AddressBuilder("5700", "Svendborg")
                .addAddressLine("Hjertemedicinsk afdeling X")
                .addAddressLine("Valdemarsgade 53x")
                .setCountry("Danmark")
                .setUse(AddressData.Use.WorkPlace)
                .build())
        .setSOR("88878685")
        .addTelecom(Use.WorkPlace, "tel", "55555555")
        .setTime(at1000onJan13)
        .setPersonIdentity(authenticatorIdentity)
        .setOrganizationIdentity(authenticatorOrgIdentity)
        .build());

    // use the QFDDXmlConverter to create the XML representation
    asString = createQFDDDFromCDA(cda);
  }

  /* Smoke test using the service*/
  @Test
  public void shouldValidate() {
	  final ValidationResponse response = ClientBuilder.newClient().validateCdaType(CdaType.QFDD.name()).postXmlAsJson(asString, ValidationResponse.class);
	  assertEquals(0, response.getErrors().size());
  }
}
