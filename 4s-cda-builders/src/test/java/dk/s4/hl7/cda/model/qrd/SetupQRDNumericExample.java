package dk.s4.hl7.cda.model.qrd;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.testutil.Setup;

/**
 * Helper methods to create Numeric example
 * 
 * @author Frank Jacobsen, Systematic
 *
 */
public class SetupQRDNumericExample {

  public static QRDDocument defineAsCDA() {
    QRDDocument defineQRDDocumentHeaderInformation = defineQRDDocumentHeaderInformation();
    return defineQRDDocumentHeaderInformation;
  }

  /** Define a CDA for Numeric example */
  public static QRDDocument defineQRDDocumentHeaderInformation() {

    // Define the 'time'
    Date documentCreationTime = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(UUID.randomUUID().toString())
        .setRoot(MedCom.ROOT_OID)
        .build();
    QRDDocument qrdDocument = new QRDDocument(idHeader);
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("KOL spørgeskema");

    // 1.1 Populate with time and version info
    qrdDocument.setDocumentVersion("2358344", 1);
    qrdDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qrdDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qrdDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = HelperMethods.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qrdDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = HelperMethods.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = HelperMethods.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qrdDocument.setDocumentationTimeInterval(from, to);

    String text = "Testing.";

    Section<QRDResponse> qRDSection = new Section<QRDResponse>("Indledning", text);

    CodedValue codeValue = new CodedValue("q4", "Continua-Q-QID", "Some-Display-Name", "Some-CodeSystem-Name");

    ID id = new ID.IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build();

    QRDResponse qRDResponseBuild = new QRDNumericResponse.QRDNumericResponseBuilder()
        .setInterval("1", "100", IntervalType.IVL_INT)
        .setValue("2014", BasicType.INT)
        .setCodeValue(codeValue)
        .setId(id)
        .setQuestion("ob2")
        .build();

    // External reference
    CodedValue qfddCodedValue = new CodedValueBuilder()
        .setCode(Loinc.QFD_CODE)
        .setCodeSystem(Loinc.OID)
        .setDisplayName(Loinc.DISPLAYNAME)
        .build();
    ID qfddId = new ID.IDBuilder().setRoot(MedCom.ROOT_OID).setExtension("externalDocumentReference").build();
    Reference reference1 = new Reference.ReferenceBuilder(
        Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(), qfddId, qfddCodedValue).build();
    // External reference with observation
    CodedValue phmrCodedValue = new CodedValueBuilder()
        .setCode(Loinc.PHMR_CODE)
        .setCodeSystem(Loinc.OID)
        .setDisplayName(Loinc.DISPLAYNAME)
        .build();
    ID idDocRef = new ID.IDBuilder().setRoot(MedCom.ROOT_OID).setExtension("externalDocumentReference").build();
    ID idObsRef = new ID.IDBuilder()
        .setRoot(MedCom.DK_REFERENCE_EXTERNAL_OBSERVATION)
        .setExtension("externalObservationReference")
        .build();
    Reference reference2 = new Reference.ReferenceBuilder(
        Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(), idDocRef, phmrCodedValue)
        .externalObservation(idObsRef)
        .build();

    qRDResponseBuild.addReference(reference1);
    qRDResponseBuild.addReference(reference2);

    qRDSection.addQuestionnaireEntity(qRDResponseBuild);

    qrdDocument.addSection(qRDSection);

    return qrdDocument;
  }
}
