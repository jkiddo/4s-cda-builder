package dk.s4.hl7.cda.model.qrd;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.QRDXmlConverter;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

/**
 * Validate the output from our own QRD builder with the example from MedCom.
 *
 * @author Frank Jacobsen, Systematic
 *
 */
public final class TestAgainstMedComExamples {

  @Test
  public void shouldMatchExpectedValueEx1() throws ParserConfigurationException, SAXException, IOException {
    // Create an QRD document matching MedCom Example 1
    ClinicalDocument cda = SetupQrdKolExample1.defineAsCDA();

    // Load the MedCom example 1, MODIFIED as there are some defects in it
    shouldMatchExpectedValue(cda, "edb802b0-2e36-11e6-bdf4-0800200c9a66", "QRD_KOL_Example_1_MaTIS.xml");
  }

  private void shouldMatchExpectedValue(ClinicalDocument cda, String uuid, String filename)
      throws ParserConfigurationException, SAXException, IOException {
    QRDXmlConverter qrdXmlConverter = new QRDXmlConverter();

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    // important! http://www.kdgregory.com/index.php?page=xml.parsing
    // factory.setNamespaceAware(true);
    DocumentBuilder xmlBuilder = factory.newDocumentBuilder();

    // Load the MedCom example
    // this requires the XML files to have an UTF-8 BOM
    Document expected = xmlBuilder.parse("src/test/resources/qrd/" + filename);

    // Generate the XML
    String xmlText = qrdXmlConverter.convert((QRDDocument) cda);

    // Parse XML to dom and add stylesheet header in xml
    Document computed = HelperMethods.parseXMLStringToDOM(xmlText, true);
    HelperMethods.addStyleSheetHeader(computed);

    assertNotNull(computed);
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expected, computed);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));

    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      String computedXml = HelperMethods.convertXMLDocumentToString(computed);
      String expectedXml = HelperMethods.convertXMLDocumentToString(expected);

      System.out.println("------------- Detailed Message --------------");
      System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
      System.out.println("------------- Expected XML ------------------");
      System.out.print(HelperMethods.indentLines(expectedXml));
      System.out.println("------------- Computed XML ------------------");
      System.out.print(HelperMethods.indentLines(computedXml));
      System.out.println("------------- End of details ----------------");
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private class DebugDifferenceListener implements DifferenceListener {

    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {

    }
  }
}
