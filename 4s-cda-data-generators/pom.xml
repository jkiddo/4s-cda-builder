<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>dk.s4.hl7</groupId>
	<artifactId>datagenerators</artifactId>
	<version>1.0.1</version>
	<packaging>jar</packaging>

	<name>4S CDA data generators</name>
	<description>CDA Data generators supporting the Danish profiles</description>

	<properties>
		<!-- Source file encoding -->
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<slf4jVersion>1.7.7</slf4jVersion>
	</properties>

	<repositories>
		<repository>
			<id>4S-artifactory</id>
			<name>4S Artifactory</name>
			<url>http://artifactory.4s-online.dk/artifactory/libs-release-local</url>
		</repository>
	</repositories>

	<!-- Dependencies -->
	<dependencies>
		<dependency>
			<groupId>dk.s4.hl7</groupId>
			<artifactId>builders</artifactId>
			<version>1.0.0</version>
		</dependency>
		<!-- Apache Commons CSV -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-csv</artifactId>
			<version>1.4</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/joda-time/joda-time -->
		<dependency>
			<groupId>joda-time</groupId>
			<artifactId>joda-time</artifactId>
			<version>2.3</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-simple</artifactId>
			<version>1.7.7</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4jVersion}</version>
		</dependency>
		<dependency>
			<groupId>org.net4care</groupId>
			<artifactId>xds-connector</artifactId>
			<version>0.1.0</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-autoconfigure</artifactId>
			<version>1.1.5.RELEASE</version>
		</dependency>
		<dependency>
			<groupId>commons-cli</groupId>
			<artifactId>commons-cli</artifactId>
			<version>1.3.1</version>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>1.10.19</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<!-- Java version -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
			</plugin>
			<!-- Eclipse settings -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<additionalConfig>
						<!-- Hack to set source encoding in eclipse -->
						<file>
							<name>.settings/org.eclipse.core.resources.prefs</name>
							<content>
                <![CDATA[eclipse.preferences.version=1${line.separator}encoding/<project>=${project.build.sourceEncoding}${line.separator}]]>
							</content>
						</file>
					</additionalConfig>
				</configuration>
			</plugin>
			<!-- Plugins for generating jars containing source code and javadoc -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<version>2.9.1</version>
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<configuration>
							<failOnError>false</failOnError>
						</configuration>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.codehaus.mojo</groupId>
				<artifactId>cobertura-maven-plugin</artifactId>
				<version>2.7</version>
			</plugin>
			<plugin>
				<artifactId>maven-resources-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>copy-resources</id>
						<phase>package</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<includeEmptyDirs>true</includeEmptyDirs>
							<outputDirectory>${basedir}/target</outputDirectory>
							<resources>
								<resource>
									<directory>/src/test/resources</directory>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<version>2.2-beta-4</version>
				<configuration>
					<descriptorRefs>
						<descriptorRef>jar-with-dependencies</descriptorRef>
					</descriptorRefs>
				</configuration>
				<executions>
					<execution>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>
</project>
