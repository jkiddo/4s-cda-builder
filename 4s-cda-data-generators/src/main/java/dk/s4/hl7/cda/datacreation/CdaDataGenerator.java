package dk.s4.hl7.cda.datacreation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

/**
 * Class that generates a sigle CSV file with all the header information, from
 * difrent csv files.
 */
public class CdaDataGenerator extends DataCreationBase {

  public static void main(String[] args) throws Exception {
    CdaDataGenerator cdaDataGenerator = new CdaDataGenerator();
    cdaDataGenerator.readProperty(args);
    cdaDataGenerator.parseCsvFile();
  }

  @Override
  protected void parseCsvFile() throws IOException {
    List<CSVRecord> patientRecords = getCsvRecords(headerPath + "01_patient.csv");
    List<CSVRecord> organizationRecords = getCsvRecords(headerPath + "01_organization_input.csv");
    List<CSVRecord> custodianRecords = getCsvRecords(headerPath + "01_custodian.csv");
    List<CSVRecord> legalAuthenticatorRecords = getCsvRecords(headerPath + "01_legalauthenticator.csv");
    RingBuffer<CSVRecord> organizationRingBuffer = new RingBuffer<CSVRecord>(organizationRecords, true);
    RingBuffer<CSVRecord> legalAuthenticatorRingBuffer = new RingBuffer<CSVRecord>(legalAuthenticatorRecords, true);
    RingBuffer<CSVRecord> custodianRingBuffer = new RingBuffer<CSVRecord>(custodianRecords, true);

    List<String> cdaHeaderOutputHeaderList = new ArrayList<String>();
    cdaHeaderOutputHeaderList.add(copyHeaderFromEachCSV(organizationRecords, legalAuthenticatorRecords,
        custodianRecords, patientRecords));
    for (int patientRecordIndex = 1; patientRecordIndex < patientRecords.size(); patientRecordIndex++) {
      StringBuilder builder = new StringBuilder();
      listToCSVTextFormat(organizationRingBuffer.next(), builder);
      listToCSVTextFormat(legalAuthenticatorRingBuffer.next(), builder);
      listToCSVTextFormat(custodianRingBuffer.next(), builder);
      listToCSVTextFormat(patientRecords.get(patientRecordIndex), builder);
      builder.setLength(builder.length() - 1); // Remove last ';'
      cdaHeaderOutputHeaderList.add(builder.toString());
    }
    writeCsvFile(cdaHeaderOutputHeaderList);
  }

  private void listToCSVTextFormat(CSVRecord organizationRecord, StringBuilder builder) {
    for (int i = 0; i < organizationRecord.size(); i++) {
      builder.append(organizationRecord.get(i) + ";");
    }
  }

  private void writeCsvFile(List<String> cdaHeaderOutputHeaderList) throws IOException {
    String outputFileName = generatedCdaHeaderDataPath + "generatedCdaHeaderDataFile.csv";
    final File outputFile = new File(outputFileName);
    outputFile.getParentFile().mkdirs();
    Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputFile.getAbsolutePath()),
        "UTF-8"));

    String m_outHeader = cdaHeaderOutputHeaderList.get(0);

    CSVFormat outFormat = CSVFormat.EXCEL.withHeader(m_outHeader);
    CSVPrinter outPrinter = new CSVPrinter(new PrintWriter(writer), outFormat);
    final String newLine = String.format("%n");
    for (int i = 1; i < cdaHeaderOutputHeaderList.size(); i++) { // skip header
      writer.write(cdaHeaderOutputHeaderList.get(i));
      writer.write(newLine);
    }

    outPrinter.flush();
    outPrinter.close();

  }

}
