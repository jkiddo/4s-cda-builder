package dk.s4.hl7.cda.datacreation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Properties;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;

/**
 * Base class that contains general methods for all the classes in this package.
 * 
 */
public abstract class DataCreationBase {
  protected String generatedQrdXmlFilesPath = null;
  protected String qrdFilesPath = null;
  protected String generatedQrdDataFilesPath = null;
  protected String generatedCdaHeaderDataPath = null;
  protected String headerPath = null;
  protected String generatedPhmrDataFilesPath = null;
  protected String phmrFilesPath = null;
  protected String generatedPhmrXmlFilesPath = null;
  protected String qrdNumberDocumentsPerPatient = null;
  protected String phmrNumberDocumentsPerPatient = null;
  protected String phmrTidspunkt = null;
  protected String qrdTidspunkt = null;
  protected String phmrAntalDageMellemMaaling = null;
  protected String qrdAntalDageMellemMaaling = null;

  protected abstract void parseCsvFile() throws IOException;

  protected List<CSVRecord> getCsvRecords(String filePath) throws IOException {
    final InputStreamReader reader = new InputStreamReader(new FileInputStream(new File(filePath)), "UTF-8");
    CSVParser parser = CSVFormat.EXCEL.withDelimiter(';').parse(reader);
    List<CSVRecord> records = parser.getRecords();
    parser.close();
    reader.close();
    return records;
  }
  
  protected List<CSVRecord> getCsvRecords(String filePath, String encoding, String...header) throws IOException {
    final InputStreamReader reader = new InputStreamReader(new FileInputStream(new File(filePath)), "UTF-8");
    CSVParser parser = CSVFormat.EXCEL.withDelimiter(';').withHeader(header).parse(reader);
    List<CSVRecord> records = parser.getRecords();
    parser.close();
    reader.close();
    return records;
  }

  void readProperty(String[] args) throws IOException, URISyntaxException {
    String configPropertyPath = "Invalid path";
    if (args.length >= 1) {
      configPropertyPath = args[0];
    } else {
      configPropertyPath = new File("datacreation/").getAbsolutePath() + '/';
      System.out.println("Use default property path: " + configPropertyPath);
    }
    
    Properties prop = loadProperties(configPropertyPath);
    
    qrdNumberDocumentsPerPatient = getProperty(prop, "qrd_antal_documents_per_patient");
    phmrNumberDocumentsPerPatient = getProperty(prop, "phmr_antal_documents_per_patient");
    phmrTidspunkt = getProperty(prop, "phmr_tidspunkt");
    qrdTidspunkt = getProperty(prop, "qrd_tidspunkt");
    qrdAntalDageMellemMaaling = getProperty(prop, "phmr_antal_dage_mellem_maaling");
    phmrAntalDageMellemMaaling = getProperty(prop, "phmr_antal_dage_mellem_maaling");
    generatedQrdDataFilesPath = getProperty(prop, "generatedQrdDataFilesPath");
    generatedQrdXmlFilesPath = getProperty(prop, "generatedQrdXmlFilesPath");
    qrdFilesPath = getProperty(prop, "qrdFilesPath");
    generatedCdaHeaderDataPath = getProperty(prop, "generatedCdaHeaderDataPath");
    headerPath = getProperty(prop, "headerPath");
    generatedPhmrDataFilesPath = getProperty(prop, "generatedPhmrDataFilesPath");
    phmrFilesPath = getProperty(prop, "phmrFilesPath");
    generatedPhmrXmlFilesPath = getProperty(prop, "generatedPhmrXmlFilesPath");
  }

  private Properties loadProperties(String configPropertyPath) {
    Properties prop = new Properties();
    InputStream input = null;

    try {
      input = new FileInputStream(configPropertyPath + "config.properties");
      prop.load(input);
    } catch (IOException ex) {
      ex.printStackTrace();
    } finally {
      if (input != null) {
        try {
          input.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return prop;
  }
  
  protected String copyHeaderFromEachCSV(List<CSVRecord>...listOfListRecord) {
    StringBuilder builder = new StringBuilder();
    for (List<CSVRecord> list : listOfListRecord) {
      // Fetch first row
      if (!list.isEmpty()) {
        // copy header names
        for (String columnHeader : list.get(0)) {
          builder.append(columnHeader).append(';');
        }
      }
    }
    if (builder.length() > 0) { // remove last ';'
      builder.setLength(builder.length() - 1);
    }
    return builder.toString();
  }
  
  private String getProperty(Properties property, String propertyName) {
    if (!property.containsKey(propertyName)) {
      throw new RuntimeException("Missing property in config property file: " + propertyName);
    }
    return property.getProperty(propertyName);
  }

  protected File[] listFilesFromDirectory(String directoryName) {
    return new File(directoryName).listFiles();
  }
  
  protected static Patient definePatientPersonIdentity(CSVRecord filesDataRecord) {
   
    
    String patientCpr = filesDataRecord.get(CsvColumnNames.PATIENT_CPR.getColumnName());
    String patientFornavn = filesDataRecord.get(CsvColumnNames.PATIENT_FORNANVN.getColumnName());
    String patientEfternavn = filesDataRecord.get(CsvColumnNames.PATIENT_EFTERNAVN.getColumnName());
    String patientVejnavn =  filesDataRecord.get(CsvColumnNames.PATIENT_VEJNAVN.getColumnName());
    String patientPostnummer = filesDataRecord.get(CsvColumnNames.PATIENT_POSTNUMMER.getColumnName());
    String patientBy  = filesDataRecord.get(CsvColumnNames.PATIENT_BY.getColumnName());
    String patientTlf  = filesDataRecord.get(CsvColumnNames.PATIENT_TLF.getColumnName());
    String patientMail  = filesDataRecord.get(CsvColumnNames.PATIENT_MAIL.getColumnName());
    
    int year = DateTime.parse(patientCpr.substring(4,6),DateTimeFormat.forPattern("yy")).getYear();
    int month = Integer.parseInt(patientCpr.substring(2,4)) - 1;
    int day = Integer.parseInt(patientCpr.substring(0,2));
    
    Patient patient = 
        new PatientBuilder(patientEfternavn).
        setBirthTime(year, month, day).
        addGivenName(patientFornavn).
        setPersonID(patientCpr).
        setAddress(new AddressData.AddressBuilder(patientPostnummer, patientBy).
            setCountry("Danmark").
            addAddressLine(patientVejnavn).
            setUse(AddressData.Use.HomeAddress).
            build()).
        addTelecom(AddressData.Use.HomeAddress, "tel", patientTlf).
        addTelecom(AddressData.Use.WorkPlace, "mailto",patientMail).
        build();

    return patient;
  }
  
  
}
