package dk.s4.hl7.cda.datacreation;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Converts PHMR specific data and header information csv files, to new csv
 * file, with all the information.
 * 
 */
public class PhmrDataGenerator extends DataGeneratorBase {
  
  public static void main(String[] args) throws Exception {
    PhmrDataGenerator phmrDataGenerator = new PhmrDataGenerator();
    phmrDataGenerator.generatePhmrDocuments = true;
    // Load all properties
    phmrDataGenerator.readProperty(args);
    // first timestamp from properties file
    phmrDataGenerator.propertyTimestamp = DateTime.parse(phmrDataGenerator.phmrTidspunkt,
        DateTimeFormat.forPattern("yyyy-MM-dd"));
    phmrDataGenerator.parseCsvFile();
  }

  @Override
  protected void parseCsvFile() throws IOException {
    String outputFileName = generatedPhmrDataFilesPath + "PhmrDataFileFor";
    final boolean IsQrd = false;
    parseCsvFile(phmrFilesPath, outputFileName, IsQrd);
  }

  @Override
  protected void processDataRecord(List<CSVRecord> dataRecords, List<String> dataSet) {
    int recordsPerPatiens = (dataRecords.size() - 1) * documentsPerPatient;
    if (!dataRecords.isEmpty() && dataRecords.size() > 1) {
      for (int i = 0; i < personSize; i++) {
        propertyCalculatedTimestamp = propertyCalculatedTimestamp.plusDays(Integer.parseInt(phmrAntalDageMellemMaaling
            .trim()));
        String docId = UUID.randomUUID().toString();
        for (int recordIndex = 1; recordIndex < dataRecords.size(); recordIndex++) {
          int remeinder = dataSet.size() % recordsPerPatiens;
          if (remeinder == 0) {
            propertyCalculatedTimestamp = propertyTimestamp;
          }
          CSVRecord record = dataRecords.get(recordIndex);
          int antalMaalinger = Integer.parseInt(record.get(CsvColumnNames.PHMR_MAALINGER_PATIENT.getColumnName()));
          for (int j = 0; j < antalMaalinger; j++) {
            dataSet.add(manipulateData(dataRecords.get(recordIndex), docId));
          }
        }
      }
    }
  }

  /**
   * The PHMR specific data is computed when this method is called.
   * 
   * @param filesDataRecord
   *          , contains the specific PHMR data.
   * @param docId
   * @param docId
   * @return String that contains the new specific PHMR data as csv string.
   * 
   */
  protected String manipulateData(CSVRecord filesDataRecord, String docId) {
    String phmrTid = filesDataRecord.get(CsvColumnNames.PHMR_TIDSPUNKT.getColumnName());
    String phmrType = filesDataRecord.get(CsvColumnNames.PHMR_TYPE.getColumnName());
    String phmrEnhed = filesDataRecord.get(CsvColumnNames.PHMR_ENHED.getColumnName());
    String phmrVaerdi = filesDataRecord.get(CsvColumnNames.PHMR_VAERDI.getColumnName());
    String medicalDeviceCode = filesDataRecord.get(CsvColumnNames.MEDICAL_DEVICE_CODE.getColumnName());
    String medicalDeviceDisplayName = filesDataRecord.get(CsvColumnNames.MEDICAL_DEVICE_DISPLAYNAME.getColumnName());
    String manufacturerModelName = filesDataRecord.get(CsvColumnNames.MANUFACTURER_MODEL_NAME.getColumnName());
    String phmr__maalinger_patient = filesDataRecord.get(CsvColumnNames.PHMR_MAALINGER_PATIENT.getColumnName());

    String[] split = phmrVaerdi.split("#");

    String lowString = split[0];
    String highString = split[1];

    // We compute the phmrværdi for each measurement
    if (lowString.contains(",") || lowString.contains(".")) {
      lowString = lowString.replace(",", ".");
      highString = highString.replace(",", ".");
      double low = Double.parseDouble(lowString);
      double high = Double.parseDouble(highString);

      Double randomDouble = (Math.random() * (high - low)) + low;
      phmrVaerdi = new DecimalFormat("0.00", DecimalFormatSymbols.getInstance(Locale.ENGLISH)).format(randomDouble);

    } else {
      int low = Integer.parseInt(split[0]);
      int high = Integer.parseInt(split[1]);
      int randomInt = (int) ((Math.random() * (high - low)) + low);
      phmrVaerdi = "" + randomInt;
    }

    String objId = UUID.randomUUID().toString();

    // The phmrTid is computed from the properties file
    phmrTid = propertyCalculatedTimestamp.toString();

    return phmrTid + ";" + phmrType + ";" + phmrEnhed + ";" + phmrVaerdi + ";" + medicalDeviceCode + ";"
        + medicalDeviceDisplayName + ";" + manufacturerModelName + ";" + phmr__maalinger_patient + ";" + docId + ";"
        + objId + ";";
  }
}
